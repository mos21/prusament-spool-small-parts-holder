
include <parts-tube-integrale.scad>;

thread_size = "M48";
dos = 45;



module clips() {
    for(a = [0 : dos * 2 : 359]) {
        echo(a);
        rotate([0, 0, a]) {
            translate([((ring_id + clip_od) / 2) + ring_chamfer + 2, 0]) {
                clip();
            }
        }
    }
}

intersection() {
    rotate([0, 0, dos]) {
        color("orange") {
            carrier();
            difference(){
                clips();
                chamfered_tube(height = ring_height, od = ring_od - 0.01, id = ring_id + 0.01, chamfer = ring_chamfer, align = [0, 0, 1]);
            }
        }
    }

    color("red") {
        translate([0, 0, -50]) {
//            cube([100, 100, 100]);
        }
    }


}
