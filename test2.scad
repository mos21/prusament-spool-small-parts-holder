
difference(){

linear_extrude(5)
offset(delta = 3, chamfer = true)
offset(delta = -3)
square([20, 10], center=true);


rotate([90, 0, 0])
cylinder(d = 5, h = 20, center = true, $fn = 6);
    
}

