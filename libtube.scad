// NOSTL

        
module capsule(length = 20, diameter = 5, $fn = 12) {
    color("blue") {
        for(z = [-1, 1]) {
            translate([0, 0, z * ((length - diameter) / 2)]) {
                sphere(d = diameter);
            }
        }    
    }
    color("orange") {
        cylinder(h = (length - diameter), d = diameter, center = true);
    }
}


capsule(length = 50, diameter = 20, $fn = 30);


