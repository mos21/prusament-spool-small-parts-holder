// NOSTL

include <defaults.scad>;
include <peglib/defaults.scad>;

use <peglib/rail.scad>;
use <moslib/libchamfer.scad>;
use <libthread/threadlib/threadlib.scad>
use <libtube.scad>;

facets = 120;
$fn = facets;

thread_size = "M42";
thread_pitch = 2;
designator = str(thread_size, "x", str(thread_pitch));

bolt_minor_diameter = thread_specs(str(designator, "-ext"))[2];
echo(str(designator, " bolt_minor_diameter: ", bolt_minor_diameter));
echo(thread_specs(str(designator, "-ext")));
echo(thread_specs(str(designator, "-ext"))[0]);
assert(thread_pitch == thread_specs(str(designator, "-ext"))[0]);

nut_major_diameter = thread_specs(str(designator, "-int"))[2];
echo(str(designator, " nut_major_diameter: ", nut_major_diameter));

thread_turns = 3;

thread_od = nut_major_diameter + (wall_thickness * 2);

// scaling factor to prevent need for supports
vertical_scaling = 1.5;


section_height = 26;

spread = 1.00625;


wall_thickness_factor = 1;

grippie_fatness = 2.5;
grippie_nudge = 0.7;

    grippie_length = (section_height * 2) - (thread_turns * (thread_pitch * vertical_scaling) * 2) - (wall_thickness * 4);
    grippie_diameter = wall_thickness * grippie_fatness;
    

ff = 0.27;
    widifier = 0.5;

    clip_od = (((bolt_minor_diameter + wall_thickness) / 2 + widifier)  * 2) + grippie_diameter;
    clip_id =  bolt_minor_diameter + (wall_thickness / 2);
    
    clip_wall_thickness = clip_od - clip_id;
    echo("clip wall thickness: ", clip_wall_thickness);


    ring_id = 100;
    ring_od = ring_id + clip_wall_thickness; //(wall_thickness * 2);
    ring_chamfer = wall_thickness / 4;
    ring_height = grippie_length;
    
    spacer_height = 19;
    
    smallprint = false;
    
    
module tube_body(height = 20) {

    translate([-0, 0, wall_thickness / 4]) {
        difference() {
            union() {
                color("orangered") {
                    scale([1,1,vertical_scaling]) {
                        translate([0, 0, 1]) {
                            bolt(designator, turns = thread_turns, fn = facets);
                        }
                    }
                }

            }
            translate([0, 0, -5])
            cylinder(h = height * 2, d = bolt_minor_diameter - (wall_thickness * 2));
        }


        translate([0, 0, -wall_thickness / 4]) {
            color("salmon") {
                chamfered_tube(height = height * vertical_scaling, od = bolt_minor_diameter, id = bolt_minor_diameter - (wall_thickness * 2), chamfer = (wall_thickness / 4), align = [0, 0, 1] );
            }
        }
    }

}


module end_cap() {

    // true
    // false
    threads = true;
    label = true;
    cap = true;

    difference() {
        union() {

            if(threads) {
                translate([0, 0, wall_thickness]) {
                    // threadlib starts thread peaks on the workplane, with half a thread height below the workplane
                    translate([0, 0, (thread_pitch * vertical_scaling / 2)]) {
                        // twist to reveal threads during debugging
                        rotate([0, 0, 180]) {
                            scale([spread,spread,vertical_scaling]) {
                                color("fuchsia") {
                                    nut(designator, turns = thread_turns, Douter = thread_od - thread_pitch, fn = facets);
                                }
                            }
                        }
                    }
                }
            }


            if(cap) {
                color("cyan") {
                    chamfer = (wall_thickness / 4);
                    closed_chamfered_tube(height = wall_thickness + chamfer + ((thread_turns + 1) * thread_pitch * vertical_scaling), od = thread_od + ff, id = thread_od - (wall_thickness * 2) + ff, chamfer = chamfer, align = [0, 0, 1] ) ;
                }
            }

        }
        
        
        
        if(label) {
            color("yellow") {
                translate([0, 0, wall_thickness * 0.9]) {
                    linear_extrude(wall_thickness) {
                        fontsize = 4;
                        translate([0, 0, 0])
                            text(str(thread_size, "-", thread_pitch), valign = "center", halign = "center", size = fontsize, font=".SF NS Mono:style=Heavy");
                        if(!smallprint) {
                            translate([0, -6, 0])
                                text(":D", valign = "center", halign = "center", size = fontsize, font=".SF NS Mono:style=Heavy");
                            translate([0, 6, 0])
                                text("v1.0.1", valign = "center", halign = "center", size = fontsize, font=".SF NS Mono:style=Heavy");
                        }

                    }
                }
            }
        }
    }

}





module clip(side_grippies = true, tip_grippies = true, grippie_length = grippie_length) {
    
    cuff_arc = 240;

    widifier = 0.75;
    
    module grippie() {
        translate([0, 0, grippie_length / 2]) {
            chamfered_cylinder(od = grippie_diameter, height = grippie_length, chamfer = grippie_diameter / 8);
        }
    }
    


    // cuff
    belt_thickness = wall_thickness;

    difference() {
        color("pink") {
            chamfered_tube(od = clip_od, id =  clip_id,  height = grippie_length, chamfer = grippie_diameter / 8 );
        }

        color("lightgreen") {
            translate([0, 0, -section_height]) {
                linear_extrude(section_height * 2) {
                    polygon(points=[[0, 0], [300, 300 * sqrt(3)], [300, -300 * sqrt(3)]]);
                }
            }
        }
    }
    
    
    
    if(side_grippies) {
        // side grippies
        intersection () {
            union() { 
                for(y = [-1, 1]) {
                    translate([0, y * ((bolt_minor_diameter + wall_thickness) / 2 + widifier), 0]) {
                        color("deepskyblue") {
                            translate([0, 0, -1 * grippie_length * 1/2]) {
                                grippie();
                            }
                        }
                    }
                }
            }
            color("blue") {
                chamfered_cylinder(od = clip_od, height = grippie_length, chamfer = grippie_diameter / 8 );
            }
        }
    }
    
    if(tip_grippies) {
        // tip grippies
        foo = (clip_od - clip_id) / 2;
        echo("foo", foo);
        echo("grippie_diamater", grippie_diameter);
        for(a = [-cuff_arc/2 , cuff_arc/2])
        rotate([0, 0, 90 + a])
        translate([0, clip_id / 2, 0])
        {
                color("orangered") hull() {
                color("red") {
                    translate([0, foo/2, -1 * grippie_length * 1/2]) {
                        translate([0, 0, grippie_length / 2]) {
                            chamfered_cylinder(od = foo, height = grippie_length, chamfer = grippie_diameter / 8);
                        }
                    }
                }
                
                translate([0, foo * 0.75, 0]) {
                    color("orangered") {
                        translate([0, foo/2, -1 * grippie_length * 1/2]) {
                            translate([0, 0, grippie_length / 2]) {
                                chamfered_cylinder(od = foo, height = grippie_length, chamfer = grippie_diameter / 8);
                            }
                        }
                    }
                }
            }
        }
    }



}



module fat_little_pips() {
    translate([0, 0, -ring_height/2]) {
        for(a = [22.5 : 45 : 359]) {
            rotate([90, 0, a]) {
                scale([0.5, 0.5, 1]) {
                    cylinder(h = ring_od, d = ring_height/2, $fn = 6); 
                }
            }
        }
    }
}


module thin_little_pips() {
    intersection() {
        chamfered_tube(height = ring_height, od = ring_od - (ring_chamfer * 2), id = ring_id + (ring_chamfer * 2), chamfer = ring_chamfer, align = [0, 0, -1]);
        translate([0, 0, -ring_height/2]) {
            for(a = [22.5 : 45 : 359]) {
                rotate([90, 0, a]) {
                    scale([0.5, 0.5, 1])
                    cylinder(h = ring_od, d = ring_height/2, $fn = 6);
                }
            }
        }
    }
}
    
    
    
    
module big_pips() {
//    intersection() {
//            chamfered_tube(height = ring_height, od = ring_od, id = ring_id, chamfer = ring_chamfer, align = [0, 0, 0]);
    for(a = [0 : 90 : 359]) {
        rotate([90, 0, a]) {
            scale([1.5, 0.75, 1]) {
                cylinder(h = ring_od, d = ring_height/2);
            }
        }
    }
}



    
module half_ring(h = ring_height / 2) {
    chamfered_tube(height = h, od = ring_od, id = ring_id, chamfer = ring_chamfer, align = [0, 0, -1]);
}

module ring(h = ring_height) {
    chamfered_tube(height = h, od = ring_od, id = ring_id, chamfer = ring_chamfer, align = [0, 0, 0]);
}




module carrier() {

    
    module pips() {
        intersection() {
            chamfered_tube(height = ring_height, od = ring_od, id = ring_id, chamfer = ring_chamfer, align = [0, 0, 0]);
            for(a = [0 : 90 : 359]) {
                rotate([90, 0, a]) {
                    scale([1.5, 0.75, 1])
                    cylinder(h = ring_od, d = ring_height/2);
                }
            }
        }
    }
    
    
    
    difference() {
        color("darkmagenta") {
            half_ring();
        }
        
        color("plum") {
            rotate([0, 0, 45]) {
                big_pips();
            }
        }

        color("orangered") {
            fat_little_pips();
        }
    }
    intersection() {
        color("magenta") {
            ring();
        }
       color("blue") {
            big_pips();
        }
    }

}


module spacer() {

    rotate([180, 0, 0]) {
        color("cyan") {
            chamfered_tube(height = spacer_height, od = ring_od, id = ring_id, chamfer = ring_chamfer, align = [0, 0, -1]);
        }
        color("orangered") {
            translate([0, 0, -spacer_height]) {
                translate([0, 0, ring_height/2]) {
                    thin_little_pips();
                }
            }
        }
    }
}





// true
// false

slice = false;

do_tube = false;
do_cap = false;
do_clip = false;
do_carrier = false;
do_spacer = false;

intersection() {

    if (slice) {
        translate([-50, 0, -50]) {
            color("red") {
                cube([100, 100, 100]);
            }
        }
    }

    union() {
        if (do_tube) {
            translate([-(nut_major_diameter), nut_major_diameter, 0])
            union() {
                tube_body(height = section_height);
                translate([0, 0, section_height * 2]) {
                    rotate([180, 0, 0]) {
                        tube_body(height = section_height);
                    }
                }
            }
        }

        if (do_cap) {
            translate([(-nut_major_diameter), -nut_major_diameter, 0]) 
            union() {
                end_cap();
            }
        }
        
        if (do_clip) {
            translate([nut_major_diameter, -nut_major_diameter, 0]) 
            union() {
                clip();
            }
        }
        
        
        if (do_carrier) {
            translate([ring_od * (2/3) + 2, ring_od * (2/3) + 2, spacer_height + ring_height/2]) 
            union() {
                carrier();
            }
        }


        if (do_spacer) {
            translate([ring_od * (2/3), ring_od * (2/3), 0]) 
            union() {
                spacer();
            }
        }

    }
}

