    
include <parts-tube.scad>;

difference() {
color("cyan") {
    spacer();
}

union() {
    for(a = [0 : 45 : 359]) {
        rotate([0, 0, a]) {
            translate([(ring_id + thread_od + wall_thickness) / 2, 0, 0]) {
                cylinder(h = ring_height * 2, d = thread_od , center = true);
            }
        }
    }
}

}