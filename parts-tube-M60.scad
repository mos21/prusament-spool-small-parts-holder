
include <parts-tube-M38.scad>;

thread_size = "M60";

facets = 360;
$fn = facets;



                    translate([0, 0, -section_height]) {
                        make_tubey();
                        make_capey();
                    }
                    translate([0, 0, section_height]) {
                        rotate([180, 0, 0]) {
                            make_capey();
                        }
                    }
