
include <parts-tube.scad>;

thread_size = "M40";
dos = 45;


module clips() {
    for(a = [dos  : dos * 2 : dos ]) {
        echo(a);
        rotate([0, 0, a]) {
            translate([((ring_id + clip_od) / 2) + ring_chamfer + 2, 0]) {
                clip(side_grippies = false, tip_grippies = true, grippie_length = spacer_height);
                
                nut_major_diameter = thread_specs(str(designator, "-int"))[2];
                
biteme = nut_major_diameter / 3;
                
                translate([biteme, 0, 0]) {
                    foot = 70 ; 

                    chamfered_box(dim = [clip_wall_thickness/2, foot, spacer_height], align = [0, 0, 0], chamfer = clip_wall_thickness/8);
                    for(i = [-1, 1]) {
                        translate([0, i * foot / 2, 0]) {
                            chamfered_cylinder(spacer_height, clip_wall_thickness / 2 + 1, align = [0, 0, 0], chamfer = clip_wall_thickness / 8);
                        }
                    }

                }
            }
        }
    }
}

module clips2() {
    for(a = [0  : dos : 300 ]) {
        echo(a);
        rotate([0, 0, a]) {
            translate([((ring_id + clip_od) / 2) + ring_chamfer + 2, 0]) {
                clip(side_grippies = false, tip_grippies = false, grippie_length = spacer_height);
  
            }
        }
    }
}

intersection() {
    rotate([0, 0, dos]) {
        color("orange") {
translate([0, 0, -spacer_height/2]) {
    
    
    
    
    

    
    

    
    difference() {
color("cyan") {
    spacer();
}

union() {
    for(a = [90 : 45 : 360]) {
        rotate([0, 0, a]) {
            translate([(ring_id + thread_od + wall_thickness) / 2, 0, 0]) {
                cylinder(h = ring_height * 2, d = thread_od , center = true);
            }
        }
    }
}

}
    

    
    /*
    spacer();
    */
}            union(){
                clips();
 //  clips2();
            }
        }
    }

    color("red") {
        translate([0, 0, -50]) {
        //    cube([100, 100, 100]);
        }
    }


}
