include <parts-tube-M30.scad>;


// true
// false

smooth = true;

//facets = 360;
$fn = facets;

default_fn = facets;

tubey = false;
capey = true;

thread_size = "M40";
thread_pitch = 2;

section_height = 18;

how_many_grippies = 10;

