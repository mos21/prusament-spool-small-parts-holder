
include <parts-tube.scad>;

//translate([0, .75, 0])
intersection() {
    //cube([100, 100, 50], center = true);
    
    difference() {

        union() {
            tube_body(height = section_height);
            translate([0, 0, section_height * 2]) {
                rotate([180, 0, 0]) {
                    tube_body(height = section_height);
                }
            }
        }


        // grippies

        how_many_grippies = 12;

        if(how_many_grippies) {
            for(a = [0 : 360 / how_many_grippies: 359]) {
                rotate([0, 0, a]) {
                    translate([(bolt_minor_diameter / 2) + (wall_thickness /2) + grippie_nudge, 0, (section_height)]) {
                        color("green") {
                            capsule(length = (section_height * 2) - (thread_turns * (thread_pitch * vertical_scaling) * 2) - (wall_thickness * 3), diameter = wall_thickness * grippie_fatness);
                        }
                    }
                }
            }
        }
    }
}

