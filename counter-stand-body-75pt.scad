include <parts-tube-M30.scad>;

s = 0.75;

//debug = true;

if(!is_undef(debug)) {
    translate([-100, 0, 0]){

        color("fuchsia") intersection() {
            translate([0, -100, 0]) color("red") cube([100, 200, 100]);
            scale([s, s, s]) import("counter-stand.stl");
        }

        color("cyan") translate([-5, 60, 3 * 2]) intersection() {
            translate([0, -100, 0]) color("red") cube([100, 200, 100]);
            scale([s, s, s]) import("HPT_Base_2_digits_v3.stl");
        }

    }
}


// true
// false

facets = 360;
$fn = facets;

tubey = true;
capey = false;

thread_size = "M115";
thread_pitch = 2;

section_height = 19;

how_many_grippies = 32;

// cube([167,167,175], center = true); // maximum print size x-y


