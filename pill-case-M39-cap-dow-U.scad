include <parts-tube-M30.scad>;


// true
// false

smooth = true;

//facets = 18;
$fn = facets;

default_fn = facets;

tubey = false;
capey = true;

thread_size = "M39";
thread_pitch = 2;

section_height = 18;

how_many_grippies = 10;

outer_label = "U";

dow = true;
//dow_slug = true;
