use <threadlib/threadlib.scad>


intersection() {
    color("cyan") {
        scale([1,1,1.5]) {
            translate([0, 0, 2/2]) {
                bolt("M42x2", turns = 9);
            }
        }
    }
    translate([-50, 0, -50]) {
        cube([100,100,100]);
    }
}



color("orange") {
    scale([1,1,1]) {
        translate([0, 0, 3/2]) {
            bolt("M42x3", turns = 6);
        }
    }
}
