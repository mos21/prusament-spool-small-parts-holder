
include <parts-tube.scad>;

thread_od = nut_major_diameter + (wall_thickness * 2) + 0.27;

chamfer = (wall_thickness / 4);

grippie_height = wall_thickness + chamfer + ((thread_turns + 1) * thread_pitch * vertical_scaling);

grippie_tallness = 2/3;

how_many_grippies = 12;


// true
// false
capslice = false;

module cap() {

    difference() {

        end_cap();

        if(how_many_grippies) {
            for(a = [0 : 360 / how_many_grippies: 359]) {
                rotate([0, 0, a]) {
                    translate([(thread_od / 2) + (wall_thickness / 2) + grippie_nudge, 0, (grippie_height / 2)]) {
                        color("orange") {
                            capsule(length = grippie_height * grippie_tallness, diameter = wall_thickness * grippie_fatness);
                        }
                    }

                }
            }
        }
    }

}



difference() {

    cap();

    if(capslice) {
        translate([0, 0, grippie_height / 2])
        translate([0, 0, 50])
        cube([100, 100, 100], center = true);
    }

}

