//NOSTL

include <parts-tube.scad>;

    



// true
// false

tubey = false;
capey = false;


thread_size = "M30";

        how_many_grippies = 12;


if (tubey) {
    difference() {

        union() {
            tube_body(height = section_height);
            translate([0, 0, section_height * 2]) {
                rotate([180, 0, 0]) {
                    tube_body(height = section_height);
                }
            }
        }


        // grippies


        if(how_many_grippies) {
            for(a = [0 : 360 / how_many_grippies: 359]) {
                rotate([0, 0, a]) {
                    translate([(bolt_minor_diameter / 2) + (wall_thickness /2) + grippie_nudge, 0, (section_height)]) {
                        color("green") {
                            capsule(length = (section_height * 2) - (thread_turns * (thread_pitch * vertical_scaling) * 2) - (wall_thickness * 3), diameter = wall_thickness * grippie_fatness, $fn = $fn);
                        }
                    }
                }
            }
        }
    }
    
}


module cap() {

    difference() {

        end_cap();

        if(how_many_grippies) {
            for(a = [0 : 360 / how_many_grippies: 359]) {
                rotate([0, 0, a]) {
                    translate([(thread_od / 2) + (wall_thickness / 2) + grippie_nudge, 0, (grippie_height / 2)]) {
                        color("orange") {
                            capsule(length = grippie_height * grippie_tallness, diameter = wall_thickness * grippie_fatness, $fn = is_undef(default_fn) ? 12 : default_fn);
                        }
                    }

                }
            }
        }
    }

}

thread_od = nut_major_diameter + (wall_thickness * 2) + 0.27;

chamfer = (wall_thickness / 4);

grippie_height = wall_thickness + chamfer + ((thread_turns + 1) * thread_pitch * vertical_scaling);

grippie_tallness = 2/3;


capslice = false;



if (capey) {

    if(is_undef(dow) || dow != true) {

        translate([(-nut_major_diameter), -nut_major_diameter, 0]) 
        union() {




            difference() {

                cap();

                if(!is_undef(outer_label)) {
                    color("red")
                    translate([0, 0, -9])  {
                        linear_extrude(10) {
                            fontsize = 20;
                            translate([0, 0, 0]) {
                                rotate([0, 180, 0]) {
                                    text(str(outer_label), valign = "center", halign = "center", size = fontsize, font=".SF NS Mono:style=Heavy");
                                }
                            }
                        }
                    }
                }
                if(capslice) {
                    translate([0, 0, grippie_height / 2])
                    translate([0, 0, 50])
                    cube([100, 100, 100], center = true);
                }


            }

        }
    }
    else 
    { 
            if(is_undef(dow_slug) || dow_slug != true) {

        translate([(-nut_major_diameter), -nut_major_diameter, 0]) 
        union() {




            difference() {

                cap();

                if(!is_undef(outer_label)) {
                    color("red")
                    translate([0, 0, -9])  {
                        linear_extrude(10) {
                            fontsize = 20;
                            translate([0, 0, 0]) {
                                rotate([0, 180, 0]) {
                                    text(str(outer_label), valign = "center", halign = "center", size = fontsize, font=".SF NS Mono:style=Heavy");
                                }
                            }
                        }
                    }
                }
                if(capslice) {
                    translate([0, 0, grippie_height / 2])
                    translate([0, 0, 50])
                    cube([100, 100, 100], center = true);
                }

                translate([0, 0, 1]) {
                    cylinder(d=bolt_minor_diameter, h=wall_thickness);
                }

            }

        }
    } 
    else 
    {
                translate([(-nut_major_diameter), -nut_major_diameter, 0]) 

                            cylinder(d=bolt_minor_diameter, h=wall_thickness - 1);

        
    }
    
        
    }
}  
