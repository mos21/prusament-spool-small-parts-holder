//NOSTL
include <parts-tube-M38.scad>;

thread_size = "M52";
dos = 45;

// true
// false

limit = false;

arc_limit = limit ? 89 : 359;




module clips() {
    for(a = [0 : dos * 2 : arc_limit]) {
        echo(a);
        rotate([0, 0, a]) {
            translate([((ring_id + clip_od) / 2) + ring_chamfer + 2, 0]) {
                clip();
            }
        }
    }
}

intersection() {
    union() {
        rotate([0, 0, 0]) {
            color("orange") {
                carrier();

                difference(){
                    clips();
                    chamfered_tube(height = ring_height, od = ring_od - 0.01, id = ring_id + 0.01, chamfer = ring_chamfer, align = [0, 0, 1]);
                }

            }
        }

        rotate([180, 0, 45]) {
            color("salmon") {
                carrier();

                rotate([180, 0, 0]) {
                    difference(){
                        clips();
                        chamfered_tube(height = ring_height, od = ring_od - 0.01, id = ring_id + 0.01, chamfer = ring_chamfer, align = [0, 0, 1]);
                    }
                }
            }
        }
        

        /*
        color("orchid") {
            translate([0, 0, -(spacer_height + ring_height/2)]) {
                spacer();
            }
            translate([0, 0, (spacer_height + ring_height/2)]) {
            rotate([180, 0, 0]) {
                    spacer();
                }
            }
        }
        */


        for(a = [0 : dos : arc_limit]) {
            color("tomato") rotate([0, 0, a]) {

                translate([((ring_id + clip_od) / 2) + ring_chamfer + 2, 0, 0]) {
                    translate([0, 0, -section_height]) {
                        make_tubey();
                        make_capey();
                    }
                    translate([0, 0, section_height]) {
                        rotate([180, 0, 0]) {
                            make_capey();
                        }
                    }
                }

            }
        }
        
    }
    
    
    
    color("red") {
        translate([0, 0, -50]) {
        //    cube([100, 100, 100]);
        }
    }


}
